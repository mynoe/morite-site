const handleScroll = () => {
    let scrollTop =
        window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop;
    // let offsetTop = document.querySelector(".header").offsetTop;
    //设置背景颜色的透明读
    if (scrollTop) {
        $('.mrt-header')[0].style.background = 'rgba(0,0,0.7)'
        $('.mrt-header-search-wrapper')[0].style.background = 'rgba(225,225,225,.7)'
    } else if (scrollTop == 0) {
        $('.mrt-header')[0].style.background = 'transparent'
        $('.mrt-header-search-wrapper')[0].style.background = 'rgba(0, 0, 0, .5)'
    }
}
window.addEventListener("scroll", handleScroll);

// 首页搜索框
let searchInput = $('.mrt-header-search-wrapper')[0]
$('.mrt-header-search-wrapper').hover(function () {
    searchInput.style.visibility = 'visible'
    searchInput.style.width = '220px'
}, function () {
    searchInput.style.visibility = 'hidden'
    searchInput.style.width = '50px'
})

// 轮播  没有轮播的页面这个不要
let sss = $('.swiper .swiper-container')
console.log('sss', sss)
if ($('.swiper .swiper-container').length) {
    var swiper = new Swiper('.swiper .swiper-container', {
        autoplay: true,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
        },
    });
}



//  产品中心tab切换 product-tab-item-active
$('.product-tab .product-tab-item').on('click', function () {
    // 给当前选中的li添加一个选中的样式，除了点击当前的这个样式其他的删除样式
    $(this).addClass('product-tab-item-active').siblings().removeClass('product-tab-item-active');
    $(this).parent().parent().next().children().eq($(this).index()).removeClass('hide').siblings().addClass('hide');
})
// 解决方案tab切换 resolve-schema-tab-item-active
$('.resolve-schema-tab .resolve-schema-tab-item').on('click', function () {
    // 给当前选中的li添加一个选中的样式，除了点击当前的这个样式其他的删除样式
    $(this).addClass('resolve-schema-tab-item-active').siblings().removeClass('resolve-schema-tab-item-active');
    $(this).parent().prev().children().eq($(this).index()).removeClass('hide').siblings().addClass('hide');
})
//  新闻中心tab切换  news-center-tab-item-active
$('.news-center-tab .news-center-tab-item').on('click', function () {
    // 给当前选中的li添加一个选中的样式，除了点击当前的这个样式其他的删除样式
    $(this).addClass('news-center-tab-item-active').siblings().removeClass('news-center-tab-item-active');
    $(this).parent().next().children().eq($(this).index()).removeClass('hide').siblings().addClass('hide');
})

// mobile 菜单
$('.mrt-header-mobile-menu').on('click', function () {
    $('.mobile-menu-modal').toggle('slow')
})

// 资质证书
$('.cert-item').on('click', function () {
    let imgUrl = $(this).find('.cert-img').children('img').attr('src')
    console.log('img', imgUrl)
    $('.cert-modal').find('.cert-img').children('img').attr('src', imgUrl)
    let modal = $('.cert-modal.hide')[0]
    console.log('modal', modal)
    // modal.style.display = 'block !important'
    $('.cert-modal').addClass('show').removeClass('hide')
})

// -------
$('.cert-modal-inner').on('click', function () {
    console.log('cert-modal-inner')
    $('.cert-modal').removeClass('show').addClass('hide')
})
$('.close-btn').on('click', function (e) {
    console.log('close-btn')
    e.preventDefault() || e.stopPropagation();

    $('.cert-modal').removeClass('show').addClass('hide')
})

//----