### 文件

- index.html 首页
- product_detail.html 产品详情 done
- product_list.html 产品列表 done
- common_knowledge.html 常见百科 done
- unstanderd_design.html 非标定制 done
- contact_us.html 关于我们-联系我们 done
- enterprise_env.html 关于我们-工厂环境 done
- enterprise_intro.html 关于我们-工厂介绍 done
- enterprise_cert.html 关于我们-荣誉资质 done
- resolve_schema.html 解决方案 done
- schema_detail.html 解决方案 -详情 done
- industry_news.html 新闻列表 done
- news_detail.html 新闻 -详情 done

### 注意

- 由于历史原因，所有页面需要统一引入`reset.css`,`common.css`,`index.css`
- 然后，🧍 各个页面引入自己文件名对应的`page.name.css`
- 首页引入`index.css`
- 设计稿基于 1920px 的大屏，暂时没对窄屏做适配
- 导航部分 用 index.html 中的导航即可 ， 引入 site.js 中 mobile 菜单 切换效果

### 修改

- 导航效果
-

### 其他注意事项

- 非标定制界面中 根据后台配置的图标

```
<style>
  .zdy {
    background-image: url('./assets/icons/pic_01.png');
  }

  .unstanded-design-item:hover .zdy {
    background-image: url('./assets/icons/pic_001.png');
  }
</style>
```
